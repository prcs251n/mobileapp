package com.example.matthew.loginjsontest;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Player 2 on 09/05/2017.
 */

public class GoogleJSONConnections {
    public static final int Directions = 0;
    public static final int DistanceMatrix = 1;

    private String myUrl;
    private int dt;
    private MapsActivity MA;
    private MyDeliverysActivity MDA;
    private DeliveryRequestsActivity RA;


    public GoogleJSONConnections(int decodeType, MapsActivity ma, String url) {
        this.dt = decodeType;
        this.MA = ma;
        this.myUrl = url;
    }

    public GoogleJSONConnections(int decodeType, MyDeliverysActivity mda, String url) {
        this.dt = decodeType;
        this.MDA = mda;
        this.myUrl = url;

    }

    public GoogleJSONConnections(int decodeType, DeliveryRequestsActivity ra, String url) {
        this.dt = decodeType;
        this.RA = ra;
        this.myUrl = url;
    }

    public void execute() {
        DownloadTask DT = new DownloadTask();
        // Start downloading json data from Google Directions API
        DT.execute(myUrl);
    }


    //A method to download json data from url
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exept.: downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    /**
     * A class to download data from Google Directions URL
     */
    public class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (dt == GoogleJSONConnections.Directions) {
                DirectionsParserTask parserTask = new DirectionsParserTask();

                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
            } else if (dt == GoogleJSONConnections.DistanceMatrix) {
                MatrixParserTask parserTask = new MatrixParserTask();

                // Invokes the thread for parsing the JSON data
                parserTask.execute(result);
            }
        }
    }


    /**
     * A class to parse the Google Directions in JSON format
     */
    private class MatrixParserTask extends AsyncTask<String, Object, LinkedList<String>> {

        // Parsing the data in non-ui thread
        @Override
        protected LinkedList<String> doInBackground(String... jsonData) {

            JSONObject jObject;

            LinkedList<String> distances = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                MatrixJSONParser parser = new MatrixJSONParser();
                // Starts parsing data

                distances = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return distances;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(LinkedList<String> result) {
            if (MDA != null) {
                MDA.UpdateVisualDistances(result);
            } else if (RA != null) {
                RA.UpdateVisualDistances(result);
            }

        }

    }


    /**
     * A class to parse the Google Directions in JSON format
     */
    private class DirectionsParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);

                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jObject);


            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            int size;
            if (result != null) {
                size = result.size();
            } else {
                size = 0;
            }
            // Traversing through all the routes
            for (int i = 0; i < size; i++) {
                points = new ArrayList<LatLng>();


                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }


            }
            if (points != null) {
                MA.DrawPath(points);
            }
        }

    }


}
