package com.example.matthew.loginjsontest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Player 2 on 09/05/2017.
 */

public class MatrixJSONParser {
    public LinkedList<String> parse(JSONObject jObject) {

        LinkedList<String> distances = new LinkedList<>();

        try {
            JSONArray jRows = jObject.getJSONArray("rows");
            for (int i = 0; i < jRows.length(); i++) {

                JSONArray jElements = ((JSONObject) jRows.get(i)).getJSONArray("elements");

                for (int j = 0; j < jElements.length(); j++) {

                    String distance = (String) ((JSONObject) ((JSONObject) jElements.get(j)).get("distance")).get("text");
                    distances.add(distance);
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }
        return distances;
    }
}
