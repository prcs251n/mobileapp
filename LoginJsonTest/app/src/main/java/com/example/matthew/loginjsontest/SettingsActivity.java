package com.example.matthew.loginjsontest;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Created by Matthew on 27/03/2017.
 */

public class SettingsActivity extends AppCompatActivity implements View.OnClickListener, ResponceListener {
    FloatingActionButton FABback;
    Button BTNChangeEmail;
    Button BTNChangePassword;
    Button BTNChangeVehicle;
    AlertDialog d = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);


        FABback = (FloatingActionButton) findViewById(R.id.fabBack);
        FABback.setOnClickListener(this);

        BTNChangeEmail = (Button) findViewById(R.id.buttonChangeEmail);
        BTNChangeEmail.setOnClickListener(this);

        BTNChangePassword = (Button) findViewById(R.id.buttonChangePass);
        BTNChangePassword.setOnClickListener(this);


        BTNChangeVehicle = (Button) findViewById(R.id.buttonVehicleType);
        String[] veh = getResources().getStringArray(R.array.vehicle_types);
        BTNChangeVehicle.setText("Vehicle Type:\n" + veh[GlobalVariables.getCurrentVehicleIndex()]);
        BTNChangeVehicle.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabBack:
                onBackPressed();
                break;
            case R.id.buttonChangePass:
                ChangePass();
                break;
            case R.id.buttonVehicleType:
                ChangeVehicle();
                break;
            case R.id.buttonChangeEmail:
                ChangeEmail();
                break;

        }
    }

    private void ChangePass() {


        AlertDialog.Builder passDialog = new AlertDialog.Builder(this);
        passDialog.setView(R.layout.change_pass_layout)
                .setTitle("Change Password")
                .setCancelable(false);


        passDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
                //prevents dialog to close on button click

            }
        });
        passDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogToastResult("Action Cancelled");
                dialog.dismiss();
            }
        });


        final AlertDialog dialog = passDialog.create();
        this.d = dialog;
        dialog.show();

        final EditText oldPass = (EditText) dialog.findViewById(R.id.EToldPass);
        final EditText newPass = (EditText) dialog.findViewById(R.id.ETnewPass);
        final EditText confirmPass = (EditText) dialog.findViewById(R.id.ETconfirmPass);

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> LSTCngPass = new LinkedList<>();
                LSTCngPass.add(oldPass.getText().toString());
                LSTCngPass.add(newPass.getText().toString());
                LSTCngPass.add(confirmPass.getText().toString());

                DoChangePSWD(LSTCngPass);


            }
        });
    }

    private void DoChangePSWD(List<String> params) {
        String stringUrl = "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251N/api/staffs/" + GlobalVariables.getDriverID();
        MiddlewareConnections thread = new MiddlewareConnections(stringUrl, this, MiddlewareConnections.PSW_CHANGE_TASK, params);
        thread.start();
    }

    private void DoChangeEML(List<String> params) {
        String stringUrl = "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251N/api/staffs/" + GlobalVariables.getDriverID();
        MiddlewareConnections thread = new MiddlewareConnections(stringUrl, this, MiddlewareConnections.EML_Change_Task, params);
        thread.start();
    }

    private void DialogToastResult(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private void ChangeEmail() {


        AlertDialog.Builder passDialog = new AlertDialog.Builder(this);
        passDialog.setView(R.layout.change_email_layout)
                .setTitle("Change Email")
                .setCancelable(false);

        passDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Do nothing
                //prevents dialog to close on button click

            }
        });
        passDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DialogToastResult("Action Cancelled");
                dialog.dismiss();
            }
        });


        final AlertDialog dialog = passDialog.create();
        this.d = dialog;
        dialog.show();
        final EditText oldEmail = (EditText) dialog.findViewById(R.id.oldEmail);

        final EditText newEmail = (EditText) dialog.findViewById(R.id.newEmail);

        final EditText confirmEmail = (EditText) dialog.findViewById(R.id.confirmEmail);


        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> LSTCngEmal = new LinkedList<>();
                LSTCngEmal.add(oldEmail.getText().toString());
                LSTCngEmal.add(newEmail.getText().toString());
                LSTCngEmal.add(confirmEmail.getText().toString());


                DoChangeEML(LSTCngEmal);

            }
        });

    }

    private void ChangeVehicle() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Set the dialog title
        builder.setTitle("Update vehicle type")
                .setSingleChoiceItems(R.array.vehicle_types, GlobalVariables.getCurrentVehicleIndex(), null)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int selectedPosition = ((AlertDialog) dialog).getListView().getCheckedItemPosition();
                        UpdateVehicle(selectedPosition);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();

    }

    private void UpdateVehicle(int vehArrayIndex) {
        String[] veh = getResources().getStringArray(R.array.vehicle_types);
        boolean result = GlobalVariables.setCurrentVehicleIndex(vehArrayIndex, veh.length);

        if (result) {
            Toast.makeText(this, "Vehicle up dated to: " + veh[vehArrayIndex], Toast.LENGTH_SHORT).show();
            BTNChangeVehicle.setText("Vehicle Type:\n" + veh[GlobalVariables.getCurrentVehicleIndex()]);
        } else {
            Toast.makeText(this, "Error: Index Out Of Range", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onHTTPResponseReceived(String response) {
        d.dismiss();
        Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
    }
}
