package com.example.matthew.loginjsontest;

import android.app.AlertDialog;

import android.content.DialogInterface;

import android.graphics.Paint;

import android.location.Location;
import android.location.LocationListener;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.LinkedList;


/**
 * Created by Player 2 on 26/04/2017.
 */

public class MapsActivity extends FragmentActivity implements GoogleMap.OnCameraMoveListener, GoogleMap.OnMapClickListener, View.OnClickListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    private GoogleMap mMap;


    private Marker currentSelectedMarker;
    private Polyline polyline;

    private FloatingActionButton fabDeliver;

    private static final String PIZZA_MARKER_TAG = "Pizza Store";
    public static final String AllorONEorder = "ALL_OR_ONE";
    public static final String ALL = "ALL";
    private LinkedList<LatLng> deliveryLocations;

    // Create a stroke pattern of a gap followed by a dot.
//    private static final List<PatternItem> PATTERN_POLYLINE_DOTTED = Arrays.asList(GAP, DOT);


    private class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.map_info_window_view, null);

        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            String markerTag = (String) marker.getTag();
            TextView tempTV;

            //title: orderNo from marker tag
            tempTV = ((TextView) myContentsView.findViewById(R.id.textViewOrderNo));
            tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            tempTV.setText(markerTag);

            tempTV = ((TextView) myContentsView.findViewById(R.id.textViewCustomerName));
            tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

            if (markerTag.equals(PIZZA_MARKER_TAG)) {
                tempTV.setText(null);
                tempTV.setVisibility(View.GONE);
            } else {
                tempTV.setVisibility(View.VISIBLE);
                tempTV.setText(marker.getTitle());
            }
            tempTV = ((TextView) myContentsView.findViewById(R.id.textViewAddress));
            tempTV.setText(marker.getSnippet());

            return myContentsView;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        FloatingActionButton FABback;
        deliveryLocations = new LinkedList<>();

        TextView mode = (TextView) findViewById(R.id.textViewCurrentMode);
        String[] veh = getResources().getStringArray(R.array.vehicle_types);
        mode.setText("Current Mode:\n" + veh[GlobalVariables.getCurrentVehicleIndex()]);

        FABback = (FloatingActionButton) findViewById(R.id.fabBack);
        FABback.setOnClickListener(this);

        fabDeliver = (FloatingActionButton) findViewById(R.id.fabDeliver);
        fabDeliver.setOnClickListener(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
        mMap.setOnMapClickListener(this);
        mMap.setOnCameraMoveListener(this);
        mMap.setOnMarkerClickListener(this);


        Marker tempMarker;
        tempMarker = mMap.addMarker(new MarkerOptions()
                .position(GlobalVariables.PIZZA_SHOP)
                .title("Pizza Store")
                .snippet("Travel here THEN accept the orders")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher_home)));
        tempMarker.setTag(PIZZA_MARKER_TAG);

        //showOrderType:
        //      ALL for all in my deliveries
        //          or the order id of a single order
        String showOrderType = getIntent().getStringExtra(AllorONEorder);
        String orderIDErrors = "";

        if (showOrderType.equals(ALL)) {
            Order tempOrd;
            for (int i = 0; i < GlobalVariables.GetAmountOfOrders(true); i++) {
                tempOrd = GlobalVariables.GetOrder(true, i);
                if (!this.AddMarker(tempOrd)) {
                    orderIDErrors += ", " + tempOrd.getOrderNumber();
                }
            }
        } else {
            if (!this.AddMarker(GlobalVariables.GetOrder(true, showOrderType))) {
                orderIDErrors += ", " + showOrderType;
            }
        }

        if (!orderIDErrors.equals("")) {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setCancelable(false);
            alertDialog.setTitle("Error With Addresses with order numbers:");
            alertDialog.setMessage("Error with order Numbers " + orderIDErrors + "\n\nTry Reloading the map");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }

        DoPolylineExecution();


        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(GlobalVariables.UpdateLocation(this), 15.0f));

    }

    private void DoPolylineExecution() {
        String myUrl = getDirectionsUrl(GlobalVariables.UpdateLocation(this), GlobalVariables.PIZZA_SHOP);
        GoogleJSONConnections googleJsonConnections = new GoogleJSONConnections(GoogleJSONConnections.Directions, this, myUrl);
        googleJsonConnections.execute();
    }

    public void DrawPath(ArrayList<LatLng> points) {

        PolylineOptions PLO = new PolylineOptions();
        PLO.addAll(points);
        PLO.width(3);
        PLO.color(getResources().getColor(R.color.red));
        polyline = mMap.addPolyline(PLO);
    }

    //a method to create the url
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        String str_waypoints = "waypoints=optimize:true|";
        LatLng tempLatLong;
        for (int i = 0; i < deliveryLocations.size(); i++) {
            tempLatLong = deliveryLocations.get(i);
            str_waypoints += tempLatLong.latitude + "," + tempLatLong.longitude;

            if (i != deliveryLocations.size() - 1) {
                str_waypoints += "|";
            }
        }


        //Travel mode
        String[] veh = getResources().getStringArray(R.array.vehicle_types);
        String str_mode = "mode=" + veh[GlobalVariables.getCurrentVehicleIndex()].toLowerCase();

        //API Key
        String str_key = "key=" + getResources().getString(R.string.google_maps_key);

        // Sensor enabled
        //String sensor = "sensor=false";
        //+"&"+sensor

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + str_waypoints + "&" + str_mode + "&" + str_key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }


    private Boolean AddMarker(Order tempOrd) {
        boolean result = false;

        LatLng myLatLong = GlobalVariables.getLocationFromAddress(tempOrd.getFormattedAddress(), this);

        if (myLatLong != null) {
            Marker tempMarker = mMap.addMarker(new MarkerOptions()
                    .position(myLatLong)
                    .title(tempOrd.getCustomerName())
                    .snippet(tempOrd.getFormattedAddress()));
            tempMarker.setTag(tempOrd.getOrderNumber());
            deliveryLocations.add(myLatLong);
            result = true;
            //marker title = customer name
            //marker snippet = customer address
            //marker tag = order number
        }

        return result;
    }


    @Override
    public void onMapClick(LatLng latLng) {
        currentSelectedMarker = null;
        DoShowHideDeliverFab();
    }

    @Override
    public void onCameraMove() {
        DoShowHideDeliverFab();
    }

    private void DoShowHideDeliverFab() {
        if (currentSelectedMarker != null && currentSelectedMarker.getTag() != PIZZA_MARKER_TAG && IsMarkerInMapBounds()) {
            fabDeliver.setVisibility(View.VISIBLE);
        } else {
            fabDeliver.setVisibility(View.GONE);
        }
    }

    private boolean IsMarkerInMapBounds() {
        if (mMap.getProjection().getVisibleRegion().latLngBounds.contains(currentSelectedMarker.getPosition())) {
            return true;
        } else {
            return false;
        }
    }


    private void ShowFullOrderDetails() {

        final Order tempOrd = GlobalVariables.GetOrder(true, (String) currentSelectedMarker.getTag());

        LayoutInflater inflater = LayoutInflater.from(this);
        View viewShowAllInfo = inflater.inflate(R.layout.full_order, null);

        final AlertDialog showAllInfo = new AlertDialog.Builder(this).create();

        TextView tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewOrderNo);
        tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tempTV.setText(tempOrd.getOrderNumber());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewCustomerName);
        tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tempTV.setText(tempOrd.getCustomerName());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewDistance);
        tempTV.setText(tempOrd.getDistanceFromRider());

        ImageView imageViewTemp = (ImageView) viewShowAllInfo.findViewById(R.id.imageViewPriorities);
        imageViewTemp.setImageLevel(tempOrd.getUrgency());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewAddress);
        tempTV.setText(tempOrd.getFormattedAddress());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewNotes);
        tempTV.setText(tempOrd.getExtraNotes());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewItems);
        tempTV.setText(tempOrd.getOrderItems());

        FloatingActionButton fabCancel = (FloatingActionButton) viewShowAllInfo.findViewById(R.id.fabFOrderCancel);
        fabCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllInfo.dismiss();
            }
        });

        FloatingActionButton fabDeliver = (FloatingActionButton) viewShowAllInfo.findViewById(R.id.fabFOrderConfirm);
        fabDeliver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoDelivery(tempOrd.getOrderNumber());
                showAllInfo.dismiss();

            }
        });

        showAllInfo.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        showAllInfo.setView(viewShowAllInfo);
        showAllInfo.setCancelable(false);
        showAllInfo.setCanceledOnTouchOutside(false);
        showAllInfo.show();
    }


    private void DoDelivery(String ordNo) {
        boolean result = GlobalVariables.DeliverOrder(ordNo);

        if (result) {
            Toast.makeText(this, ordNo + " Has Delivered\nUpdating Map", Toast.LENGTH_SHORT).show();
            deliveryLocations.remove(currentSelectedMarker.getPosition());
            currentSelectedMarker.remove();
            currentSelectedMarker = null;
            DoShowHideDeliverFab();
            polyline.remove();
            DoPolylineExecution();


        } else {
            Toast.makeText(this, ordNo + ": Error in delivery", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        this.currentSelectedMarker = marker;
        DoShowHideDeliverFab();


        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabBack:
                finish();
                break;
            case R.id.fabDeliver:
                ShowFullOrderDetails();
                break;
        }
    }


}
