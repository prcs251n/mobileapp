package com.example.matthew.loginjsontest;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Matthew on 14/03/2017.
 */

public class MainMenuActivity extends AppCompatActivity implements View.OnClickListener {

    TextView TVTopBar;

    Button BTNRequests;
    Button BTNMyDeliverys;
    Button BTNSettings;
    Button BTNMap;
    FloatingActionButton FABback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        TVTopBar = (TextView) findViewById(R.id.textViewTitle);


        BTNRequests = (Button) findViewById(R.id.buttonDeliveryRequests);
        BTNRequests.setOnClickListener(this);

        BTNMyDeliverys = (Button) findViewById(R.id.buttonMyDeliverys);
        BTNMyDeliverys.setOnClickListener(this);

        BTNMap = (Button) findViewById(R.id.buttonMap);
        BTNMap.setOnClickListener(this);

        BTNSettings = (Button) findViewById(R.id.buttonSettings);
        BTNSettings.setOnClickListener(this);

        FABback = (FloatingActionButton) findViewById(R.id.fabBack);
        FABback.setOnClickListener(this);

        String name = GlobalVariables.getName();
        TVTopBar.setText("Welcome, " + name + "!");

        ShowInfoMessagebox();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonDeliveryRequests:
                Toast.makeText(this, "Loading Requests", Toast.LENGTH_SHORT).show();
                Intent requests = new Intent(this, DeliveryRequestsActivity.class);
                startActivity(requests);
                break;
            case R.id.buttonMyDeliverys:
                Toast.makeText(this, "Loading Deliveries", Toast.LENGTH_SHORT).show();

                Intent MyDeliverys = new Intent(this, MyDeliverysActivity.class);
                startActivity(MyDeliverys);
                break;

            case R.id.buttonMap:
                Toast.makeText(this, "Loading Map", Toast.LENGTH_SHORT).show();
                Intent Map = new Intent(this, MapsActivity.class);
                Map.putExtra(MapsActivity.AllorONEorder, MapsActivity.ALL);
                startActivity(Map);
                break;
            case R.id.buttonSettings:
                Intent settings = new Intent(this, SettingsActivity.class);
                startActivity(settings);
                break;
            case R.id.fabBack:
                Logout();
                break;
        }

    }



    @Override
    public void onBackPressed() {
        Logout();
    }

    private void ShowInfoMessagebox() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Logged In!")
                .setMessage(getResources().getString(R.string.GPS_reminder))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        builder.show();


    }

    private void SuperLogout() {
        super.onBackPressed();
        GlobalVariables.SetName("");
        GlobalVariables.setDriverID(-1);
        LoginActivity.ClearTXTBoxes();
    }

    private void Logout() {
        Boolean deliveriesRemaining = (GlobalVariables.GetAmountOfOrders(true) > 0);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (deliveriesRemaining) {
            builder.setTitle("You Have Deliveries Remaining")
                    .setMessage(R.string.LogoutError)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            builder.setTitle("Are you sure you want to log out?")
                    .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            SuperLogout();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        }

        builder.show();


    }
}
