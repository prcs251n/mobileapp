package com.example.matthew.loginjsontest;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by Player 2 on 10/05/2017.
 */

public class ChangePSWDParser {
    public static final String PSWD_CHANGED = "Password Updated";


    public String parse(JSONObject jStaff, List<String> PSWDetails, String StaffUrlToChange) {

        boolean validNewPass = IsValidPassword(PSWDetails.get(1));

        String oldUserPSWD = doSHA3(PSWDetails.get(0));
        String newPSWD = doSHA3(PSWDetails.get(1));
        String confirmPSWD = doSHA3(PSWDetails.get(2));

        String result = "Not Changed";

        try {
            String oldPass = (String) jStaff.get("STAFFPASSWORD").toString();
            //user entered password = current password
            if (oldPass.equals(oldUserPSWD)) {

                //user entered new passwords are equal
                if (newPSWD.equals(confirmPSWD)) {

                    //new password matches the criteria
                    if (validNewPass) {


                        JSONObject newJStaff = jStaff;
                        newJStaff.put("STAFFPASSWORD", newPSWD);

                        result = DoUpdatePassword(newJStaff, StaffUrlToChange);


                    } else {
                        result = "Password does not meet criteria";
                    }
                } else {
                    result = "Passwords do not match";
                }

            } else {
                result = "Old Password Wrong";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return result;
    }


    private String DoUpdatePassword(JSONObject newJStaff, String urlToChange) {

        try {

            URL myUrl = new URL(urlToChange);


            HttpURLConnection httpCon = (HttpURLConnection) myUrl.openConnection();
            httpCon.setRequestMethod("PUT");
            httpCon.setDoOutput(true);
            httpCon.setRequestProperty("User-Agent", "Mozilla/5.0");
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");

            OutputStreamWriter os = new OutputStreamWriter(httpCon.getOutputStream());
            os.write(newJStaff.toString());
            os.flush();
            os.close();

            int code = httpCon.getResponseCode();
            if (code == 204) {
                return PSWD_CHANGED;
            } else {
                return "Error: " + code;
            }


        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }


    }


    private String doSHA3(String pass) {
        String Hash = "";

        try {
            String Salted = pass + GlobalVariables.SALT;

            MessageDigest digest = null;
            digest = MessageDigest.getInstance("SHA512");

            byte[] output = digest.digest(Salted.getBytes("UTF-8"));


            for (byte b : output) {
                Hash += String.format("%02X ", b);
                Hash = Hash.trim();
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return Hash;

    }

    private boolean IsValidPassword(String p) {
        boolean valid = false;
        if (p.length() >= 4) {
            boolean containsUpper = p.matches(".*[A-Z].*");
            boolean containsLower = p.matches(".*[a-z].*");
            boolean containsNumber = p.matches(".*[0-9].*");
            boolean noSpecial = !p.matches(".*[#$%^&*\"\'].*");

            if (containsLower && containsUpper && containsNumber && noSpecial) {
                valid = true;
            }
        }

        return valid;
    }

}
