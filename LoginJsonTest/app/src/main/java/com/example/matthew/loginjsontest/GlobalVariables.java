package com.example.matthew.loginjsontest;


import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.Nullable;

import com.google.android.gms.common.stats.StatsEvent;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.security.PublicKey;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by medarvill on 14/03/2017.
 */

public class GlobalVariables {
    // TODO: Remove when adding MIDDLEWARE CONNECTION
    private static String name = "";
    private static int driverID = -1;


    public static String PASS_CHANGED = "Password Changed";
    public static String EMAIL_CHANGED = "Email Changed";

    public static final String SALT = "IDmJnbTTlzkMASTYs7kHsYusrvub0vzSVtyXUTtVgWx8UHCMK3Y4Zk8n41jc";

    //TODO: add variables to store:

    //TODO: ordering linked list of objects by a object variable


    private static LatLng lastKnownLatLong = null;

    public static final LatLng PIZZA_SHOP = new LatLng(50.375300, -4.140765);
    public static final int MAX_DELIVERIES = 5;
    private static List<Order> requestingOrders = new LinkedList<>();
    private static List<Order> myOrders = new LinkedList<>();
    private static int currentVehicleIndex = 0;

    public static void AddRequestOrder(Order o ){
        requestingOrders.add(o);
    }

    public static LatLng UpdateLocation(Context c) {
        LocationManager lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        Location location = lm.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        if (location != null) {
            lastKnownLatLong = new LatLng(location.getLatitude(), location.getLongitude());
        }
        return lastKnownLatLong;
    }


    public static LatLng getLocationFromAddress(String strAddress, Context c) {

        Geocoder coder = new Geocoder(c);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            Address location = address.get(0);
            location.getLatitude();
            location.getLongitude();

            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        } catch (IndexOutOfBoundsException ex) {
            return null;
        }

        return p1;
    }

    public static int getCurrentVehicleIndex() {
        return currentVehicleIndex;
    }

    public static Boolean setCurrentVehicleIndex(int i, int maxVeh) {

        if (i >= 0 && i < maxVeh) {
            currentVehicleIndex = i;
            return true;
        } else {
            return false;
        }
    }


    public static String getName() {
        return name;
    }

    public static void SetName(String n) {
        name = n;
    }

    public static void setDriverID(int id) {
        driverID = id;
    }

    public static int getDriverID() {
        return driverID;
    }

    public static Order GetOrder(boolean isMyOrder, int orderIndex) {
        if (isMyOrder) {
            return myOrders.get(orderIndex);
        } else {
            return requestingOrders.get(orderIndex);

        }
    }

    @Nullable
    public static Order GetOrder(boolean isMyOrder, String orderNd) {
        int index = GetOrderIDIndex(isMyOrder, orderNd);

        if (isMyOrder && index != -1) {
            return myOrders.get(index);
        } else if (!isMyOrder && index != -1) {
            return requestingOrders.get(index);
        } else {
            return null;
        }
    }

    private static int GetOrderIDIndex(boolean IsMyOrder, String orderId) {
        int index = 0;
        boolean found = false;
        int size = GetAmountOfOrders(IsMyOrder);

        if (IsMyOrder) {
            while (index < size && !found) {
                if (myOrders.get(index).getOrderNumber().equals(orderId)) {
                    found = true;
                } else index++;
            }
        } else {
            while (index < size && !found) {
                if (requestingOrders.get(index).getOrderNumber() == orderId) {
                    found = true;
                } else index++;
            }
        }

        if (!found) {
            index = -1;
        }


        return index;


    }

    public static int GetAmountOfOrders(boolean isMyOrders) {
        if (isMyOrders) {
            return myOrders.size();
        } else {
            return requestingOrders.size();
        }
    }

    public static String GetOrderConfirmMessage(String orderId) {
        String output;
        int orderIndex = GetOrderIDIndex(false, orderId);
        String prio = "Unknown";
        switch (requestingOrders.get(orderIndex).getUrgency()) {
            case 0:
                prio = "Low";
                break;
            case 1:
                prio = "Medium";
                break;
            case 2:
                prio = "High";
                break;
        }
        output = "Priority: " + prio;
        output = output + "\nDistance: " + requestingOrders.get(orderIndex).getDistanceFromRider() + " Mi";
        output = output + "\nMy Deliveries: " + myOrders.size() + "/" + MAX_DELIVERIES;
        return output;
    }

    public static boolean AcceptOrder(String orderID) {


        boolean added;
        if (myOrders.size() >= MAX_DELIVERIES) {
            return false;
        }


        int index = GetOrderIDIndex(false, orderID);

        if (index == -1) {
            added = false;
        } else {
            Order tempOrd = requestingOrders.get(index);
            myOrders.add(tempOrd);
            requestingOrders.remove(index);
            added = true;
        }
        return added;

    }

    public static boolean DeclineOrder(String orderID) {

        boolean declined;
        int index = GetOrderIDIndex(true, orderID);
        if (index == -1) {
            declined = false;
        } else {
            Order tempOrd = myOrders.get(index);
            requestingOrders.add(tempOrd);
            myOrders.remove(index);
            declined = true;
        }

        return declined;
    }

    public static boolean DeliverOrder(String orderID) {
        boolean result;
        int orderIndex = GetOrderIDIndex(true, orderID);
        Order tempOrd = myOrders.remove(orderIndex);
        result = !tempOrd.equals(new Order());

        return result;
    }



    public static void CreateTestData() {


        requestingOrders.add(CreateOrderOne());
        requestingOrders.add(CreateOrderTwo());
        requestingOrders.add(CreateOrderThree());
        requestingOrders.add(CreateOrderFour());
        requestingOrders.add(CreateOrderFive());
        // requestingOrders.add(CreateOrderFive());


    }


    private static Order CreateOrderOne() {
        Order ord = new Order();

        ord.setCustomerName("Alex Care");
        ord.setAddressLine1("96 Beaumont Road");
        ord.setTown("Plymouth");
        ord.setCounty("Devon");
        ord.setPostcode("PL4 9EA");
        ord.setOrderNumber("AC0102");

        ord.AddOrderItem("Garlic Bread");
        ord.AddOrderItem("Peperoni");
        ord.AddOrderItem("2L Coke");

        ord.setDistanceFromRider("Null");
        ord.setUrgency(0);

        return ord;
    }

    private static Order CreateOrderTwo() {
        Order ord = new Order();

        ord.setCustomerName("Ben Shafto");
        ord.setAddressLine1("54 Connaught avenue");
        ord.setTown("Plymouth");
        ord.setCounty("Devon");
        ord.setPostcode(" PL4 7BU");
        ord.setOrderNumber("PL0236");
        ord.setExtraNotes("round the back");

        ord.AddOrderItem("Hawaiian");
        ord.AddOrderItem("Hawaiian");
        ord.AddOrderItem("Meat Feast");
        ord.AddOrderItem("Meat Feast");
        ord.AddOrderItem("Advacado + Grape");

        ord.setDistanceFromRider("Null");
        ord.setUrgency(2);

        return ord;
    }

    private static Order CreateOrderThree() {
        Order ord = new Order();

        ord.setCustomerName("Matt Darvill");
        ord.setAddressLine1("61a Alexandra Road");
        ord.setAddressLine2("Mutley");
        ord.setTown("Plymouth");
        ord.setCounty("Devon");
        ord.setPostcode("PL4 7EF");
        ord.setOrderNumber("MD8568");
        ord.setExtraNotes("Down the stairs");

        ord.AddOrderItem("Hawaiian");
        ord.AddOrderItem("Garlic Bread");
        ord.AddOrderItem("2L Sprite");

        ord.setDistanceFromRider("Null");
        ord.setUrgency(1);

        return ord;
    }

    private static Order CreateOrderFour() {
        Order ord = new Order();

        ord.setCustomerName("Gina HowlTon");
        ord.setAddressLine1("Drake Circus");
        //ord.setAddressLine2("");
        ord.setTown("Plymouth");
        ord.setCounty("Devon");
        ord.setPostcode("PL4 8AA");
        ord.setOrderNumber("TT0550");

        ord.AddOrderItem("Vegi");
        ord.AddOrderItem("Margarita");


        ord.setDistanceFromRider("Null");
        ord.setUrgency(0);

        return ord;
    }

    private static Order CreateOrderFive() {
        Order ord = new Order();

        ord.setCustomerName("Megan Fisher");
        ord.setAddressLine1("1 Alexandra Road");

        ord.setTown("Plymouth");
        ord.setCounty("Devon");
        ord.setPostcode("Pl4 7EF");
        ord.setOrderNumber("AA0001");

        ord.AddOrderItem("Hawaiian");
        ord.AddOrderItem("Hawaiian");
        ord.AddOrderItem("Peperoni");
        ord.AddOrderItem("Vegi");
        ord.AddOrderItem("Margarita");

        ord.AddOrderItem("Pizza Garlic Bread");
        ord.AddOrderItem("Garlic Bread");
        ord.AddOrderItem("Potato Wedges");

        ord.AddOrderItem("2L Coke");
        ord.AddOrderItem("2L Sprite");
        ord.AddOrderItem("2L Fanta");


        ord.setDistanceFromRider("Null");
        ord.setUrgency(1);

        return ord;
    }

}
