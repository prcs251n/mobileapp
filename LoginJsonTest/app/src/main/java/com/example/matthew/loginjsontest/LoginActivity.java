package com.example.matthew.loginjsontest;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements ResponceListener {

    Button BTNLogin;
    static EditText ETUser;
    static EditText ETPass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_page);




        BTNLogin = (Button) findViewById(R.id.buttonLogin);
        BTNLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DoLogin();
            }
        });


        ETUser = (EditText) findViewById(R.id.editTextUserEmail);
        ETUser.setText("");
        ETPass = (EditText) findViewById(R.id.editTextPass);
        ETPass.setText("");

       // GlobalVariables.CreateTestData();


    }

    private void DoLogin() {
        List<String> details = new LinkedList<>();
        details.add(ETUser.getText().toString());
        details.add(ETPass.getText().toString());

        String stringUrl = "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251N/api/staffs";
        MiddlewareConnections thread = new MiddlewareConnections(stringUrl, this, MiddlewareConnections.LOGIN_TASK, details);
        thread.start();
        Toast.makeText(this,"Checking Values",Toast.LENGTH_SHORT).show();
    }




    @Override
    public void onHTTPResponseReceived(String response) {
        if (response.equals(LoginParser.Valid)) {
            Intent mainMenu = new Intent(this, MainMenuActivity.class);
            startActivity(mainMenu);
        } else {
            Toast.makeText(this,response,Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public static void ClearTXTBoxes() {
        ETUser.setText("");
        ETPass.setText("");
    }


}

