package com.example.matthew.loginjsontest;


import com.google.android.gms.common.stats.StatsEvent;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Player 2 on 10/05/2017.
 */

public class LoginParser {
    public static final String Valid = "Valid";
    private static final String Failed = "Failed";
    private static final String DRIVER_CODE = "0";


    public String parse(JSONArray jStaffs, List<String> LoginDetails) {
        String result = Failed;
        try {
            boolean validEmail = false;
            boolean validPass = false;
            boolean isDriver = false;
            boolean validLogin = false;
            int staffIndex = 0;


            String userEmail = LoginDetails.get(0);
            String HashedPass = doSHA3(LoginDetails.get(1));


            //loop through every staff member
            while (staffIndex < jStaffs.length() && !validLogin) {
                validEmail = false;
                validPass = false;
                isDriver = false;

                validEmail = userEmail.equals((String) ((JSONObject) jStaffs.get(staffIndex)).get("STAFFEMAIL"));

                validPass = HashedPass.equals((String) ((JSONObject) jStaffs.get(staffIndex)).get("STAFFPASSWORD"));

                isDriver = ((String) ((JSONObject) jStaffs.get(staffIndex)).get("STAFFROLE")).equals(DRIVER_CODE);


                if (validEmail && validPass && isDriver) {
                    validLogin = true;
                } else {
                    staffIndex++;
                }
            }

            if (validLogin) {
                result = Valid;
                GlobalVariables.SetName((String) ((JSONObject) jStaffs.get(staffIndex)).get("STAFFFNAME"));
                GlobalVariables.setDriverID((int) ((JSONObject) jStaffs.get(staffIndex)).get("STAFF_ID"));
            } else {
                result = "Incorrect Details";
            }


        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
        }


        return result;
    }

    private String doSHA3(String pass) {
        String Hash = "";

        try {
            String Salted = pass + GlobalVariables.SALT;
            MessageDigest digest = null;
            digest = MessageDigest.getInstance("SHA512");

            byte[] output = digest.digest(Salted.getBytes("UTF-8"));


            for (byte b : output) {
                Hash += String.format("%02X ", b);
                Hash = Hash.trim();
            }

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return Hash;

    }


}
