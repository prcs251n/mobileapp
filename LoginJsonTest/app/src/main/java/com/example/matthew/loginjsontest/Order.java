package com.example.matthew.loginjsontest;

import com.google.android.gms.maps.model.LatLng;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Matthew on 15/03/2017.
 */

public class Order {
    private String customerName;
    private String addressLine1;
    private String addressLine2;
    private String town;
    private String county;
    private String postcode;


    private String orderNumber;
    private List<String> orderItems;
    private String extraNotes;

    private String distanceFromRider;
    private int urgency;

    public Order() {
        this.orderItems = new LinkedList<>();

        this.customerName = "Null";
        this.addressLine1 = "Null";
        this.addressLine2 = "Null";
        this.town = "Null";
        this.county = "Null";
        this.postcode = "Null";
        this.orderNumber = "Null";
        this.distanceFromRider = "Null";
        this.urgency = 0;
        this.extraNotes = "Null";


    }

    public String getExtraNotes() {
        return extraNotes;
    }

    public void setExtraNotes(String extraNotes) {
        this.extraNotes = extraNotes;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }


    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }


    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }


    public void setTown(String town) {
        this.town = town;
    }


    public void setCounty(String county) {
        this.county = county;
    }


    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getFormattedAddress() {
        String addr = "";
        addr = addr + addressLine1 + "\n";
//        if (!addressLine2.equals("Null") || !addressLine2.isEmpty() || !addressLine2.equals("null")) {
//            addr = addr + addressLine2 + "\n";
//        }

        addr = addr + this.town + "\n" + this.county + "\n" + this.postcode;

        return addr;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getOrderItems() {

        Collections.sort(orderItems, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });

        int itemAmount = 0;
        String prevItem = null;
        String output = "";

        for (String item : orderItems) {
            if (prevItem == null) {
                prevItem = item;
                itemAmount++;
            } else if (item.equals(prevItem)) {
                itemAmount++;
            } else {
                output = output + itemAmount + " * " + prevItem + "\n";
                prevItem = item;
                itemAmount = 1;
            }
        }
        output = output + itemAmount + " * " + prevItem + "\n";


        return output;

    }

    public int GetAmountOfOrderItems() {
        return orderItems.size();
    }

    public void AddOrderItem(String orderItem) {
        orderItems.add(orderItem);
    }

    public String getDistanceFromRider() {
        return distanceFromRider;
    }

    public void setDistanceFromRider(String distanceFromRider) {
        this.distanceFromRider = distanceFromRider;
    }

    public int getUrgency() {
        return urgency;
    }

    public void setUrgency(int urgency) {
        this.urgency = urgency;
    }
}
