package com.example.matthew.loginjsontest;


import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import java.net.URL;
import java.util.List;


/**
 * Created by Player 2 on 09/05/2017.
 */

public class MiddlewareConnections {
    private String URLData = "";
    private String responseString = "Nothing Happened";
    private String stringURL = null;
    private ResponceListener listener = null;
    private List<String> parameters = null;
    private WebServiceTask task = null;
    private int taskToDo = -1;

    public static final int LOGIN_TASK = 1;
    public static final int PSW_CHANGE_TASK = 2;
    public static final int EML_Change_Task = 3;
    public static final int GET_REQUESTS_TASK = 4;


    public MiddlewareConnections(String url, ResponceListener listener, int task, List<String> p) {

        this.stringURL = url;
        this.listener = listener;
        this.taskToDo = task;
        this.parameters = p;
    }

    public void start() {
        try {
            task = new WebServiceTask();
            task.execute();
        } catch (Exception wtf) {
            listener.onHTTPResponseReceived(wtf.getMessage());
        }
    }

    private void run() {

        URL url = null;
        HttpURLConnection urlConnection = null;

        try {

            url = new URL(stringURL);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);


            int data;
            while ((data = isw.read()) >= 0) {

                URLData += Character.toString((char) data);

            }
        } catch (Exception e) {
            e.printStackTrace();
            URLData += e.getMessage() + "\n" + e.toString();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        if (taskToDo == LOGIN_TASK) {
            try {
                JSONArray jArray = new JSONArray(URLData);
                LoginParser parser = new LoginParser();
                responseString = parser.parse(jArray, parameters);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (taskToDo == PSW_CHANGE_TASK) {
            try {
                JSONObject jObj = new JSONObject(URLData);
                ChangePSWDParser parser = new ChangePSWDParser();
                responseString = parser.parse(jObj, parameters, stringURL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (taskToDo == EML_Change_Task) {
            try {
                JSONObject jObj = new JSONObject(URLData);
                ChangeEMLParser parser = new ChangeEMLParser();
                responseString = parser.parse(jObj, parameters, stringURL);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if ( taskToDo == GET_REQUESTS_TASK){
            //get 5 delivery requests
            try {

                //jObj is all orders
                JSONArray jsonArray = new JSONArray(URLData);
                GetRequestsParser parser = new GetRequestsParser();
                responseString = parser.parse(jsonArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    private void returnResult() {
        if (listener != null) {
            listener.onHTTPResponseReceived(responseString);
        }

    }

    private class WebServiceTask extends AsyncTask<Void, Void, Void> {


        @Override
        protected Void doInBackground(Void... params) {
            Log.i("WebServiceTask", "Task invoked");

            run();

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {

            returnResult();
        }

    }


}
