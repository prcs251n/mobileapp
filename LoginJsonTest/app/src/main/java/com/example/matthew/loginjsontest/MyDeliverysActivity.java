package com.example.matthew.loginjsontest;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.LinkedList;

/**
 * Created by Matthew on 27/03/2017.
 */

public class MyDeliverysActivity extends AppCompatActivity implements View.OnClickListener {

    FloatingActionButton FABback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.current_deliverys);

        FABback = (FloatingActionButton) findViewById(R.id.fabBack);
        FABback.setOnClickListener(this);

        Toast.makeText(this, "my deliveries: " + GlobalVariables.GetAmountOfOrders(true), Toast.LENGTH_LONG).show();
        UpdateDistances();


        RelativeLayout RelLay;
        int amountToShow = GlobalVariables.GetAmountOfOrders(true);
        TextView TVTemp;
        String tileID, buttonID;
        ImageView IVTempPrioritys;
        Button tempButton;


        Order tempOrd;

        if (amountToShow > 5) {
            amountToShow = 5;
        }


        for (int i = 0; i < 5; i++) {
            tileID = "d" + (i + 1);

            int resID = getResources().getIdentifier(tileID, "id", getPackageName());
            RelLay = ((RelativeLayout) findViewById(resID));


            if (i >= amountToShow) {
                RelLay.setVisibility(View.GONE);
            } else {
                tempOrd = GlobalVariables.GetOrder(true, i);

                TVTemp = (TextView) RelLay.findViewById(R.id.textViewAddress);
                TVTemp.setText(tempOrd.getFormattedAddress());

                TVTemp = (TextView) RelLay.findViewById(R.id.textViewDistance);
                TVTemp.setText(tempOrd.getDistanceFromRider() + " Mi");

                TVTemp = (TextView) RelLay.findViewById(R.id.textViewItems);
                TVTemp.setText(tempOrd.GetAmountOfOrderItems() + " Items");

                TVTemp = (TextView) RelLay.findViewById(R.id.textViewCustomerName);
                TVTemp.setPaintFlags(TVTemp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                TVTemp.setText(tempOrd.getCustomerName());

                IVTempPrioritys = (ImageView) RelLay.findViewById(R.id.imageViewPriorities);
                IVTempPrioritys.setImageLevel(tempOrd.getUrgency());

                TVTemp = (TextView) RelLay.findViewById(R.id.textViewOrderNo);
                TVTemp.setPaintFlags(TVTemp.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
                TVTemp.setText(tempOrd.getOrderNumber());

                buttonID = "btn" + (i + 1) + "View";
                resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                tempButton = (Button) findViewById(resID);

                final String orderId = tempOrd.getOrderNumber();
                tempButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       ShowFullOrder(orderId);
                    }
                });

                buttonID = "btn" + (i + 1) + "Track";
                resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                tempButton = (Button) findViewById(resID);
                tempButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TrackOrder(orderId);
                    }
                });

                buttonID = "btn" + (i + 1) + "Delete";
                resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                tempButton = (Button) findViewById(resID);
                final int finalI = i;
                tempButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ConfirmDelete(orderId, finalI);
                    }
                });
            }


        }

    }

    private void ShowFullOrder(String ordID) {

        final Order tempOrd = GlobalVariables.GetOrder(true, ordID);

        LayoutInflater inflater = LayoutInflater.from(this);
        View viewShowAllInfo = inflater.inflate(R.layout.full_order, null);

        final AlertDialog showAllInfo = new AlertDialog.Builder(this).create();

        TextView tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewOrderNo);
        tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tempTV.setText(tempOrd.getOrderNumber());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewCustomerName);
        tempTV.setPaintFlags(tempTV.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tempTV.setText(tempOrd.getCustomerName());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewDistance);
        tempTV.setText(tempOrd.getDistanceFromRider());

        ImageView imageViewTemp = (ImageView) viewShowAllInfo.findViewById(R.id.imageViewPriorities);
        imageViewTemp.setImageLevel(tempOrd.getUrgency());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewAddress);
        tempTV.setText(tempOrd.getFormattedAddress());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewNotes);
        tempTV.setText(tempOrd.getExtraNotes());

        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewItems);
        tempTV.setText(tempOrd.getOrderItems());

        FloatingActionButton fabCancel = (FloatingActionButton) viewShowAllInfo.findViewById(R.id.fabFOrderCancel);
        fabCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAllInfo.dismiss();
            }
        });


        tempTV = (TextView) viewShowAllInfo.findViewById(R.id.textViewFullOrderDoAction);
        tempTV.setVisibility(View.INVISIBLE);

        FloatingActionButton fabDeliver = (FloatingActionButton) viewShowAllInfo.findViewById(R.id.fabFOrderConfirm);
        fabDeliver.setVisibility(View.INVISIBLE);

        showAllInfo.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        showAllInfo.setView(viewShowAllInfo);
        showAllInfo.setCancelable(false);
        showAllInfo.setCanceledOnTouchOutside(false);
        showAllInfo.show();
    }


    private void UpdateDistances() {
        LatLng lastLatLong = GlobalVariables.UpdateLocation(this);
        if (lastLatLong != null) {

            LinkedList<LatLng> locations = new LinkedList<>();
            LatLng tempLL = null;
            Order tempOrd;

            for (int i = 0; i < GlobalVariables.GetAmountOfOrders(true); i++) {
                tempOrd = GlobalVariables.GetOrder(true, i);
                tempLL = null;
                tempLL = GlobalVariables.getLocationFromAddress(tempOrd.getFormattedAddress(), this);
                if (tempLL != null) {
                    locations.add(tempLL);
                }
            }

            if (!locations.isEmpty()) {
                String distanceURL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + lastLatLong.latitude + "," + lastLatLong.longitude;
                String str_destinations = "&destinations=";
                LatLng tempLatLong;
                for (int i = 0; i < locations.size(); i++) {
                    tempLatLong = locations.get(i);
                    str_destinations += tempLatLong.latitude + "," + tempLatLong.longitude;
                    if (i != locations.size() - 1) {
                        str_destinations += "|";
                    }

                }
                String str_key = "&key=" + getResources().getString(R.string.google_maps_key);

                distanceURL += str_destinations + str_key;
                GoogleJSONConnections googleJsonConnections = new GoogleJSONConnections(GoogleJSONConnections.DistanceMatrix, this, distanceURL);
                googleJsonConnections.execute();
            }
        }
    }

    public void UpdateVisualDistances(LinkedList<String> result) {
        int resID;
        RelativeLayout RelLay;
        TextView distanceTV;

        for (int i = 0; i < GlobalVariables.GetAmountOfOrders(true); i++) {
            GlobalVariables.GetOrder(true, i).setDistanceFromRider(result.get(i));

            resID = getResources().getIdentifier("d" + (i + 1), "id", getPackageName());
            RelLay = ((RelativeLayout) findViewById(resID));
            distanceTV = (TextView) RelLay.findViewById(R.id.textViewDistance);
            distanceTV.setText(GlobalVariables.GetOrder(true, i).getDistanceFromRider());
        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fabBack:
                onBackPressed();
                break;

        }
    }

    private void TrackOrder(final String OrderID) {
        // Toast.makeText(this, "add method to track: " + OrderID, Toast.LENGTH_SHORT).show();

        AlertDialog.Builder trackDialog = new AlertDialog.Builder(this);
        trackDialog.setTitle("Would you like to track this Order?")
                .setMessage("Tracking individually will take you stright to the address avoiding other addresses in 'My Deliveries'" +
                        "\nThis will be the only delivery shown on the map")
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Track", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        TrackIndividual(OrderID);
                    }
                });
        trackDialog.show();

    }


    private void TrackIndividual(String OrderID) {
        Toast.makeText(this, "Loading Map", Toast.LENGTH_SHORT).show();
        Intent Map = new Intent(this, MapsActivity.class);
        Map.putExtra(MapsActivity.AllorONEorder, OrderID);
        startActivity(Map);
        this.finish();

    }


    private void ConfirmDelete(final String orderID, final int relLayIndex) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();

        alertDialog.setTitle("Remove order " + orderID + " from 'my Deliveries'?");
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Delete",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteOrder(orderID, relLayIndex);
                        dialog.dismiss();
                    }
                });

        alertDialog.show();
    }

    private void DeleteOrder(String orderID, int layoutIndex) {

        boolean deleted = GlobalVariables.DeclineOrder(orderID);


        if (deleted) {
            int tempInt = getResources().getIdentifier("d" + (layoutIndex + 1), "id", getPackageName());
            RelativeLayout RelLay = ((RelativeLayout) findViewById(tempInt));
            RelLay.setVisibility(View.GONE);
            Toast.makeText(this, "Declined order: " + orderID + "\nYour Orders: " + GlobalVariables.GetAmountOfOrders(true) + "/" + GlobalVariables.MAX_DELIVERIES, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Could not Decline order: " + orderID + "\nYour Orders: " + GlobalVariables.GetAmountOfOrders(true) + "/" + GlobalVariables.MAX_DELIVERIES, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}


