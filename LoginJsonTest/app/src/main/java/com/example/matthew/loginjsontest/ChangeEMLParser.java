package com.example.matthew.loginjsontest;

import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Player 2 on 10/05/2017.
 */

public class ChangeEMLParser {
    public static final String EML_CHANGED = "Email Updated";

    public String parse(JSONObject jStaff, List<String> EMLDetails, String StaffUrlToChange) {
        String result = "Not Changed";


        try {
            String oldEmail = (String) jStaff.get("STAFFEMAIL").toString();
            if (oldEmail.equals(EMLDetails.get(0))) {
                if (EMLDetails.get(1).equals(EMLDetails.get(2))) {
                    if (IsValidEmail(EMLDetails.get(1))) {


                        JSONObject newJStaff = jStaff;
                        newJStaff.put("STAFFEMAIL", EMLDetails.get(1));

                        result = DoUpdateEmail(newJStaff, StaffUrlToChange);


                    } else {
                        result = "Email does not meet criteria";
                    }
                } else {
                    result = "New Emails do not match";
                }

            } else {
                result = "Old Email Wrong";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return result;
    }

    private String DoUpdateEmail(JSONObject newJStaff, String urlToChange) {

        try {

            URL myUrl = new URL(urlToChange);

            HttpURLConnection httpCon = (HttpURLConnection) myUrl.openConnection();
            httpCon.setRequestMethod("PUT");
            httpCon.setDoOutput(true);
            httpCon.setRequestProperty("User-Agent", "Mozilla/5.0");
            httpCon.setRequestProperty("Content-Type", "application/json");
            httpCon.setRequestProperty("Accept", "application/json");

            OutputStreamWriter os = new OutputStreamWriter(httpCon.getOutputStream());
            os.write(newJStaff.toString());
            os.flush();
            os.close();

            int code = httpCon.getResponseCode();
            if (code == 204) {
                return EML_CHANGED;
            } else {
                return "Error: " + code;
            }


        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }


    }


    public static boolean IsValidEmail(String e) {
        boolean result = false;
        boolean isEmail = e.matches(".*@.*\\.com");
        boolean noSpecial = !e.matches(".*[#$%^&*\"\'].*");
        if (isEmail && noSpecial) {
            result = true;
        }

        return result;
    }

}
