package com.example.matthew.loginjsontest;

import android.util.JsonToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Player 2 on 11/05/2017.
 */

public class GetRequestsParser {


    public String parse(JSONArray jOrders) {
        String result = "";


        ArrayList<JSONObject> validOrders = GetValidOrders(jOrders);


        ArrayList<JSONObject> myRequestsOrders = GetMax5MyRequests(validOrders);


        Order tempOrder = new Order();
        try {
            for (JSONObject Request : myRequestsOrders) {
                tempOrder = new Order();
                int CustomerID = Request.getInt("CUSTOMERID");
                JSONObject customer = GetCustomer(CustomerID);

                tempOrder.setOrderNumber(Request.getString("ORDERID"));
                tempOrder.setCustomerName(customer.getString("CUSTFNAME") + " " + customer.getString("CUSTLNAME"));
                tempOrder.setAddressLine1(customer.getString("ADDRESSLINE1"));
                tempOrder.setAddressLine2(customer.getString("ADDRESSLINE2"));
                tempOrder.setTown(customer.getString("TOWN"));
                tempOrder.setCounty(customer.getString("COUNTY"));
                tempOrder.setPostcode(customer.getString("POSTCODE"));
                tempOrder.setExtraNotes(Request.getString("NOTES"));
                tempOrder.setUrgency(Request.getInt("PRIORITY"));

                String items = Request.get("ORDERITEMS").toString();
                tempOrder.AddOrderItem(items);
                GlobalVariables.AddRequestOrder(tempOrder);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }


        return result;
    }

    private ArrayList<JSONObject> GetMax5MyRequests(ArrayList<JSONObject> vo) {
        ArrayList<JSONObject> result = new ArrayList<JSONObject>();

        int howManyValid = vo.size();
        int index = 0;

        while (index < vo.size() && index < 5) {
            result.add(vo.get(index));

            index++;
        }


        return result;
    }

    private JSONObject GetCustomer(int customerID) {

        JSONObject jCustomer = null;

        String urlData = "";
        HttpURLConnection urlConnection = null;
        try {
            String myUrl = "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251N/api/customers/" + customerID;
            URL url = new URL(myUrl);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            InputStream in = urlConnection.getInputStream();

            InputStreamReader isw = new InputStreamReader(in);


            int data;
            while ((data = isw.read()) >= 0) {
                urlData += Character.toString((char) data);
            }
            jCustomer = new JSONObject(urlData);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return jCustomer;
    }

    private ArrayList<JSONObject> GetValidOrders(JSONArray jOrders) {

        ArrayList<JSONObject> result = new ArrayList<>();

        try {
            for (int i = 0; i < jOrders.length(); i++) {
                JSONObject obj = jOrders.optJSONObject(i);

                if (obj.getInt("ORDERSTATUS") == 1) {
                    result.add(obj);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return result;

    }
}
